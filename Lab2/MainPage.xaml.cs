﻿using Lab2.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<WowItem> items { get; set; }
        public MainPage()
        {
            InitializeComponent();

            items = new ObservableCollection<WowItem>();

            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679957676545015818/etalon_good.png", label = "Monture 1", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2"  });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label = "Monture 2", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679958698264756262/fabious_good.png", label = "Monture 3", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2"  });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679954281667100692/meca_nabab_good.png", label = "Monture 4", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2"  });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679953181639311422/serpent_naz_good.png", label = "Monture 5", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2"  });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679960786348671028/uuna_good.png", label = "Monture 6", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679962100398817280/plumes_good.png", label = "Monture 7", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679962420437057576/francois_good.png", label = "Monture 8", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679961758181752843/maccarbre_good.png", label = "Monture 9", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });
            items.Add(new WowItem { image = "https://cdn.discordapp.com/attachments/671875180724879360/679961238632923136/baal_good.png", label = "Monture 10", image2 = "https://cdn.discordapp.com/attachments/671875180724879360/679958065050681402/shadowBarb_good.png", label2 = "Monture 2" });


            listView.ItemsSource = items;
        }
    }
}
